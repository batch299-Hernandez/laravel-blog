@extends('layouts.app')

@inject('postComments', 'App\Models\PostComment')

@section('content')

    <!-- Comment Modal -->
	<div class="modal fade" id="comment-modal" tabindex="-1" aria-labelledby="comment-modal" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Comment on post</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form id="comment-form" name="comment-form" action="/posts/{{$post->id}}/comment" method="POST">
	        	@csrf
	        	<div class="form-group">
	        		<textarea class="w-100" name="content" id="content" cols="30" rows="5" placeholder="Write a comment about the post"></textarea>
	        	</div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button form="comment-form" type="submit" class="btn btn-primary">Publish Comment</button>
	      </div>
	    </div>
	  </div>
	</div>


    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>

            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>

            @if(Auth::id() != $post->user_id)
                <form action="/posts/{{$post->id}}/like" class="d-inline" method="POST">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains('user_id', Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            @if(Auth::id())
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#comment-modal">
			    Comment
			    </button>

            @endif
        </div>
    </div>

 
    <?php
		// Initialize getting all of the comments
		$comments = $postComments::where('post_id', $post->id)->get();
	?>

	@foreach($comments as $comment)
	<h3 class="mt-5">Comments</h3>
	<div class="card w-100">
		<div class="card-body">
			<h5 class="card-title">{{$comment->content}}</h3>
			<p class="card-subtitle">{{$comment->user->name}}</p>
		</div>
	</div>
	@endforeach
@endsection