@extends('layouts.app')

@section('content')
    <div class="text-center">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" width="450">
        <h3>Featured Posts</h3>
    </div>        
    @if(count($posts) > 0)
        @foreach($posts as $post)
        <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                </div>
            </div> 
        @endforeach
    @endif
@endsection